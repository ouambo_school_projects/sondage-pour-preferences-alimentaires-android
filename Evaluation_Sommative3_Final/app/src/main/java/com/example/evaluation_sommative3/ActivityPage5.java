package com.example.evaluation_sommative3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityPage5 extends AppCompatActivity {

    public static final String ID="";
    public int identifiant;
    public static String TAillE ="";
    DataBase maBase;

    private Button boutonQuitter;
    private Button boutonPrecedent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page5);

        Intent i = getIntent();
        identifiant = i.getIntExtra(ID,0);
        maBase = new DataBase(this);

        boutonPrecedent = findViewById(R.id.idBoutonPrecedentPage5);
        boutonQuitter = findViewById(R.id.idBoutonQuiterPage5);


        boutonPrecedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPage5.this, ActivityPage4.class);
                intent.putExtra(ActivityPage4.ID, identifiant);
                startActivity(intent);
            }
        });

        boutonQuitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}