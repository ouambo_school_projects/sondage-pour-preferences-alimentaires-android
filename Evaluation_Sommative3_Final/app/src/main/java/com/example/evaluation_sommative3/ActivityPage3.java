package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityPage3 extends AppCompatActivity {

    public static final String ID="";
    public int identifiant;
    public static String TAillE ="";
    DataBase maBase;

    public Resources res;
    private String[] choix;

    private int taille = 26;
    TextView textView;
    TextView textView2;
    TextView textView3;


    private boolean[] choixSelected;

    private ListView listViewChoix;

    private AdaptateurPage3 adaptateur;

    private Button boutonPrecedent;
    private Button boutonSuivant;

    private int choixSelectedDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);

        Intent i = getIntent();
        identifiant = i.getIntExtra(ID,0);
        maBase = new DataBase(this);

        res = getResources();
        choix = res.getStringArray(R.array.choix_question3);

        choixSelected = new boolean[choix.length];

        choixSelectedDataBase = maBase.getQuestion3(identifiant);
        choixSelected = getChoixSelected();

        listViewChoix = findViewById(R.id.idListViewPage3);

        adaptateur = new AdaptateurPage3(this, choix);
        listViewChoix.setAdapter(adaptateur);

        adaptateur.updateRecords(choixSelected);

        textView=findViewById(R.id.idQuestion3);
        textView2=findViewById(R.id.idBoutonPrecedentPage3);
        textView3=findViewById(R.id.idBoutonSuivantPage3);
        taille = i.getIntExtra(TAillE,0);
        //setTextSize(taille);

        //Toast.makeText(ActivityPage2.this, "province = " + provinceSelectedDataBase, Toast.LENGTH_LONG).show();

        boutonPrecedent = findViewById(R.id.idBoutonPrecedentPage3);
        boutonSuivant = findViewById(R.id.idBoutonSuivantPage3);


        listViewChoix.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean etatChoix = choixSelected[position];

                if(!etatChoix){
                    choixSelected[position] = true;
                    for(int k=0; k<choixSelected.length; k++){
                        if(k != position)
                            choixSelected[k] = false;
                    }
                    adaptateur.updateRecords(choixSelected);
                }
            }
        });

        boutonPrecedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndiceReponse();
                boolean b = maBase.updateQuestion3(identifiant, indiceReponse);
                Intent intent = new Intent(ActivityPage3.this, ActivityPage2.class);
                //intent.putExtra(ActivityPage2.TAillE, taille);
                intent.putExtra(ActivityPage2.ID, identifiant);
                startActivity(intent);
            }
        });

        boutonSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndiceReponse();
                boolean b = maBase.updateQuestion3(identifiant, indiceReponse);
                boolean b2 = maBase.updatePageCourante(identifiant, 4);
                Intent intent = new Intent(ActivityPage3.this, ActivityPage4.class);
                //intent.putExtra(ActivityPage4.TAillE, taille);
                intent.putExtra(ActivityPage4.ID, identifiant);
                startActivity(intent);
            }
        });

    }

    public int getIndiceReponse(){
        int ind = -1;
        for(int j = 0; j < choixSelected.length; j++){
            if(choixSelected[j] == true){
                ind = j;
            }
        }
        return ind;
    }

    public boolean[] getChoixSelected(){
        boolean[] tab = new boolean[choixSelected.length];
        tab = choixSelected;
        for(int k = 0; k< tab.length; k++){
            if(k == choixSelectedDataBase)
                tab[k] = true;
        }
        return tab;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            // finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;

        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }

    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTextSize(int textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }
}