package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

     DataBase maBase;
     private  Menu menu;
    public static String TAillE ="";

    public MenuItem description;
    Button buttonPetit;
    Button buttonMoyen;
    Button buttonGrand;
    Button exitbutton;
    int taille = 26; // Variable to store selected text size
    TextView textView; // TextView to display text with updated text size
    TextView textView5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        maBase = new DataBase(this);

        // Initialize TextView
        textView = findViewById(R.id.textView);
        textView5 = findViewById(R.id.textView5);

        // Retrieve the Taille value from Intent extras
        taille = getIntent().getIntExtra("taille", 20);

        // Set initial text size
        setTextSize(taille);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;
        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }



    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void PageDeConnexion(View view) {
        // Pass the selected text size to the next activity
        Intent intent = new Intent(MainActivity.this, ActivityConnexion.class);
        intent.putExtra(ActivityConnexion.TAillE, taille);
        startActivity(intent);
    }


    private void setTextSize(int textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView5.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }
}