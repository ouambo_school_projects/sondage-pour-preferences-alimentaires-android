package com.example.evaluation_sommative3;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

public class Adaptateur extends BaseAdapter {
    private int[] images;
    private String[] repas;
    private boolean[] repasSelected;
    private Context contexte;
    private LayoutInflater inflater;

    Activity activity;

    public  Adaptateur(Activity activity){
        this.activity = activity;
    }

   /* public Adaptateur(Activity activity, int[] images, String[] repas){
        this.activity = activity;
        this.images = images;
        this.repas = repas;
        this.repasSelected = new boolean[repas.length];

        inflater = activity.getLayoutInflater();
    }*/

    public Adaptateur(Context context, int[] images, String[] repas){
        this.contexte = context;
        this.images = images;
        this.repas = repas;
        this.repasSelected = new boolean[repas.length];
        this.inflater = (LayoutInflater) this.contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        View rowview = inflater.inflate(R.layout.page1_contenu, parent, false);

        TextView tvRepas = rowview.findViewById(R.id.idTextViewRepas);
        tvRepas.setText(repas[position]);

        ImageView ivRepas = rowview.findViewById(R.id.idImageRepas);
        ivRepas.setImageResource(images[position]);

        ImageView ivCheckBox = rowview.findViewById(R.id.idCheckBoxRepas);

        boolean etatRepas = repasSelected[position];

        if(etatRepas){
            ivCheckBox.setImageResource(R.drawable.checked);
        }
        else{
            ivCheckBox.setImageResource(R.drawable.check);
        }

        return rowview;


      /* ViewHolder holder = null;

       if(view == null){
           view = inflater.inflate(R.layout.page1_contenu, parent, false);
           holder = new ViewHolder();

           holder.tvRepas = (TextView) view.findViewById(R.id.idTextViewRepas);
           holder.tvRepas.setText(repas[position]);

           holder.ivRepas = (ImageView) view.findViewById(R.id.idImageRepas);
           holder.ivRepas.setImageResource(images[position]);

           holder.ivCheckBox = (ImageView) view.findViewById(R.id.idCheckBoxRepas);

           view.setTag(holder);
       }
       else{
           holder = (ViewHolder) view.getTag();
       }

       boolean etatRepas = repasSelected[position];

       if(etatRepas)
           holder.ivCheckBox.setImageResource(R.drawable.checked);
       else
           holder.ivCheckBox.setImageResource(R.drawable.check);

       return view;*/
    }

    public void updateRecords(boolean[] repasSelected){
        this.repasSelected = repasSelected;
        notifyDataSetChanged();
    }


  /*  class ViewHolder{
        TextView tvRepas;
        ImageView ivRepas;
        ImageView ivCheckBox;
    }*/

}
