package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityPage4 extends AppCompatActivity {

    public static final String ID="";
    public int identifiant;
    public static String TAillE ="";
    private int taille = 26;
    TextView textView;
    TextView textView2;
    TextView textView3;
    DataBase maBase;

    public Resources res;

    public  String[] periode;

    public boolean[] periodeSelected;

    public  int[] images = {R.drawable.matinee, R.drawable.midi, R.drawable.apres_midi, R.drawable.soiree};

    private Button boutonSuivant;
    private Button boutonPrecedent;
    ListView listViewPeriode;

    public int periodeSelectedDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page4);

        Intent i = getIntent();
        identifiant = i.getIntExtra(ID,0);
        maBase = new DataBase(this);

        res = getResources();
        periode = res.getStringArray(R.array.periode);

        periodeSelected = new boolean[periode.length];
        periodeSelectedDataBase = maBase.getQuestion4(identifiant);
        periodeSelected = getPeriodeSelected();

        listViewPeriode = (ListView) findViewById(R.id.idListViewPage4);
        final AdaptateurPage4 adaptateur = new AdaptateurPage4(this,images,periode);
        listViewPeriode.setAdapter(adaptateur);

        adaptateur.updateRecords(periodeSelected);

        textView=findViewById(R.id.idQuestion4);
        textView2=findViewById(R.id.idBoutonPrecedentPage4);
        textView3=findViewById(R.id.idBoutonSuivantPage4);

        boutonPrecedent = findViewById(R.id.idBoutonPrecedentPage4);
        boutonSuivant = findViewById(R.id.idBoutonSuivantPage4);


        listViewPeriode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean etatPeriode = periodeSelected[position];

                if(!etatPeriode){
                    periodeSelected[position] = true;
                    for(int k=0; k<periodeSelected.length; k++){
                        if(k != position)
                            periodeSelected[k] = false;
                    }
                    adaptateur.updateRecords(periodeSelected);
                }
            }
        });

        boutonPrecedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndiceReponse();
                boolean b = maBase.updateQuestion4(identifiant, indiceReponse);
                Intent intent = new Intent(ActivityPage4.this, ActivityPage3.class);
                intent.putExtra(ActivityPage3.ID, identifiant);
                startActivity(intent);
            }
        });

        boutonSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndiceReponse();
                boolean b = maBase.updateQuestion4(identifiant, indiceReponse);
                boolean b2 = maBase.updatePageCourante(identifiant, 5);
                Intent intent = new Intent(ActivityPage4.this, ActivityPage5.class);
                intent.putExtra(ActivityPage5.ID, identifiant);
                startActivity(intent);
            }
        });
    }

    public int getIndiceReponse(){
        int ind = -1;
        for(int j = 0; j < periodeSelected.length; j++){
            if(periodeSelected[j] == true){
                ind = j;
            }
        }
        return ind;
    }

    public boolean[] getPeriodeSelected(){
        boolean[] tab = new boolean[periodeSelected.length];
        tab = periodeSelected;
        for(int k = 0; k< tab.length; k++){
            if(k == periodeSelectedDataBase)
                tab[k] = true;
        }
        return tab;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            // finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;

        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }

    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTextSize(int textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }
}