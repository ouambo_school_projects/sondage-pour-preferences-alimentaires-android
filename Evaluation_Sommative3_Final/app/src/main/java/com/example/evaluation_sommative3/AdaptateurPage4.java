package com.example.evaluation_sommative3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdaptateurPage4 extends BaseAdapter {

    private int[] images;
    private String[] periode;
    private boolean[] periodeSelected;
    private Context contexte;
    private LayoutInflater inflater;

    public AdaptateurPage4(Context context, int[] images, String[] periode){
        this.contexte = context;
        this.images = images;
        this.periode = periode;
        this.periodeSelected = new boolean[periode.length];
        this.inflater = (LayoutInflater) this.contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return periode.length;
    }

    @Override
    public Object getItem(int position) {
        return periode[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowview = inflater.inflate(R.layout.page4_contenu, parent, false);

        TextView tvPeriode = rowview.findViewById(R.id.idTextViewPage4);
        tvPeriode.setText(periode[position]);

        ImageView ivPeriode = rowview.findViewById(R.id.idImageViewPage4);
        ivPeriode.setImageResource(images[position]);

        ImageView ivCheckBox = rowview.findViewById(R.id.idCheckBoxPage4);

        boolean etatPeriode = periodeSelected[position];

        if(etatPeriode){
            ivCheckBox.setImageResource(R.drawable.checked);
        }
        else{
            ivCheckBox.setImageResource(R.drawable.check);
        }

        return rowview;
    }

    public void updateRecords(boolean[] periodeSelected){
        this.periodeSelected = periodeSelected;
        notifyDataSetChanged();
    }

}
