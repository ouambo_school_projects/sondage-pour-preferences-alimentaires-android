package com.example.evaluation_sommative3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdaptateurPage3 extends BaseAdapter {

    private Context contexte;

    private String[] choix;

    private boolean[] choixSelected;

    private LayoutInflater inflater;

    public AdaptateurPage3(Context context, String[] choix){
        this.contexte = context;
        this.choix = choix;
        this.choixSelected = new boolean[choix.length];
        this.inflater = (LayoutInflater) this.contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return choix.length;
    }

    @Override
    public Object getItem(int position) {
        return choix[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowview = inflater.inflate(R.layout.page3_contenu, parent, false);

        TextView tvChoix = rowview.findViewById(R.id.idTextViewPage3);
        tvChoix.setText(choix[position]);

        ImageView ivCheckBox = rowview.findViewById(R.id.idRadioPage3);

        boolean etatChoix = choixSelected[position];

        if(etatChoix){
            ivCheckBox.setImageResource(R.drawable.radio_c);
        }
        else{
            ivCheckBox.setImageResource(R.drawable.radio);
        }

        return rowview;
    }

    public void updateRecords(boolean[] choixSelected){
        this.choixSelected = choixSelected;
        notifyDataSetChanged();
    }

}
