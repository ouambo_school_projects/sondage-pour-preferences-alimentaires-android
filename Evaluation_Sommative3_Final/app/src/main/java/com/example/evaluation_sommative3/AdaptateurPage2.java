package com.example.evaluation_sommative3;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

public class AdaptateurPage2 extends BaseAdapter {

    private int[] images;
    private String[] province;
    private Context contexte;
    private LayoutInflater inflater;

    private boolean[] provinceSelected;
    Activity activity;

   /* public AdaptateurPage2(Activity activity, int[] images, String[] province){
        this.activity = activity;
        this.images = images;
        this.province = province;
        this.provinceSelected = new boolean[province.length];
        inflater = activity.getLayoutInflater();
    } */

     public AdaptateurPage2(Context context, int[] images, String[] province){
        this.contexte = context;
        this.images = images;
        this.province = province;
        this.provinceSelected = new boolean[province.length];
        this.inflater = (LayoutInflater) this.contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

         View rowview = inflater.inflate(R.layout.page2_contenu, parent, false);

         TextView tvProvince = rowview.findViewById(R.id.idTextViewProvince);
         tvProvince.setText(province[position]);

         ImageView ivProvince = rowview.findViewById(R.id.idImageViewProvince);
         ivProvince.setImageResource(images[position]);

        ImageView ivCheckBox = rowview.findViewById(R.id.idCheckBoxProvince);

        boolean etatProvince = provinceSelected[position];

        if(etatProvince){
            ivCheckBox.setImageResource(R.drawable.radio_c);
        }
        else{
            ivCheckBox.setImageResource(R.drawable.radio);
        }

        return rowview;

       /* ViewHolder holder = null;

        if(view == null){
            view = inflater.inflate(R.layout.page2_contenu, parent, false);
            holder = new ViewHolder();

            holder.tvProvince = (TextView) view.findViewById(R.id.idTextViewProvince);
            holder.tvProvince.setText(province[position]);

            holder.ivProvince = (ImageView) view.findViewById(R.id.idImageViewProvince);
            holder.ivProvince.setImageResource(images[position]);

            holder.ivCheckBox = (ImageView) view.findViewById(R.id.idCheckBoxProvince);

            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }

        boolean etatRepas = provinceSelected[position];

        if(etatRepas)
            holder.ivCheckBox.setImageResource(R.drawable.radio_c);
        else
            holder.ivCheckBox.setImageResource(R.drawable.radio);

        return view; */

       /* View rowView = inflater.inflate(R.layout.page2_contenu, parent, false);

        int idImage = images[position];
        String nomRepas = province[position];

        ImageView imageViewProvince = rowView.findViewById(R.id.idImageViewProvince);
        imageViewProvince.setImageResource(idImage);

        TextView textViewProvince = rowView.findViewById(R.id.idTextViewProvince);
        textViewProvince.setText(nomRepas);

        return rowView; */
    }

    public void updateRecords(boolean[] provinceSelected){
        this.provinceSelected = provinceSelected;
        notifyDataSetChanged();
    }

   /* class ViewHolder{
        TextView tvProvince;
        ImageView ivProvince;
        ImageView ivCheckBox;
    } */
}
