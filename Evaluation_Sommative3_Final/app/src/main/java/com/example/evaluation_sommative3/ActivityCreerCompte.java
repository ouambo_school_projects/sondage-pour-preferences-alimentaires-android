package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityCreerCompte extends AppCompatActivity {
    DataBase maBase;
    public  String nom, email, telephone, password, password2;

    TextView textView; // TextView to display text with updated text size
    TextView textView2;
    TextView textView3;

    public static String TAillE ="";
    private int taille = 26;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_compte);
        Intent i = getIntent();
        maBase = new DataBase(this);

        textView = findViewById(R.id.textView3);
        textView2 =findViewById(R.id.textView8);
        textView3 =findViewById(R.id.textView7);

        taille = i.getIntExtra(TAillE,0);
        setTextSize(taille);
    }

    public void Connecter(View view) {
        Intent intent = new Intent(ActivityCreerCompte.this, ActivityConnexion.class);
        intent.putExtra(ActivityConnexion.TAillE, taille);
        startActivity(intent);
    }

    public void creerUtilisateur(View view) {
        nom = ((EditText)findViewById(R.id.textInputEditTextNom)).getText().toString().trim();
        email = ((EditText)findViewById(R.id.textInputEditTextEmail)).getText().toString().trim();
        telephone = ((EditText)findViewById(R.id.textInputEditTextPhone)).getText().toString().trim();
        password = ((EditText)findViewById(R.id.textInputEditTextPassword)).getText().toString().trim();
        password2 = ((EditText)findViewById(R.id.textInputEditTextRePassword)).getText().toString().trim();

        if(!ChampsObligatoiresRemplis())
            Toast.makeText(this,"Veuillez remplir tous les champs obligatoires",
                    Toast.LENGTH_SHORT).show();
        else if (!EgaliteMotsDePasse()) {
            Toast.makeText(this,"Mots de passe différents ou " +
                            "taille inférieure à 6 caractères",
                    Toast.LENGTH_SHORT).show();
        }
        else{insererUtilisateur();}
   }

   public boolean ChampsObligatoiresRemplis(){
        return  !nom.equals("") && !email.equals("") &&
                !password.equals("") && !password2.equals("");
   }

   public  boolean EgaliteMotsDePasse(){
        return password.equals(password2) && password.length() >= 6 && password2.length() >= 6;
   }

   public void insererUtilisateur(){
       //Verification pour savoir si l'utilisateur existe deja
       if(maBase.verifierEmail(email))
           Toast.makeText(this,"Cet utilisateur existe déjà",
                   Toast.LENGTH_SHORT).show();
       else{
           boolean resultInsertion = maBase.insererVisiteur(nom, email, telephone, password);
           if(resultInsertion){
               Intent i = new Intent(this, ActivityConnexion.class);
               i.putExtra(ActivityConnexion.TAillE, taille);
               i.putExtra(ActivityConnexion.EMAIL, email);
               startActivity(i);
               Toast.makeText(this,"Utilisateur ajouté avec succès. " +
                       "\nVous pouvez des à présent vous connecter.", Toast.LENGTH_SHORT).show();
           }
           else
               Toast.makeText(this,"Echec d'ajout du nouvel utilisateur",
                       Toast.LENGTH_SHORT).show();
       }
   }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            //finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;
        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }



        return super.onOptionsItemSelected(item);
    }



    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    private void setTextSize(int textSize) {

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }


}