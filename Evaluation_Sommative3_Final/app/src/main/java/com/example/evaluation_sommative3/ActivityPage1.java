package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityPage1 extends AppCompatActivity{
    public static final String ID="";
    public int identifiant;

    private int taille = 26;

    public static String TAillE ="";
    TextView textView2;
    TextView textView3;

    DataBase maBase;

    public  String[] repas;

    public boolean[] repasSelected;
    public  int[] images = {R.drawable.bacon ,R.drawable.barren_anaimo, R.drawable.bloody_aesar, R.drawable.cabane_a_sucre_feves_au_lard,
            R.drawable.canadiens_pizza, R.drawable.chaudree_de_palourde, R.drawable.gateau_chocolat  , R.drawable.guedille_homard_sandwich,
            R.drawable.huitre_poisson_fruits_de_mer, R.drawable.pain_de_viande, R.drawable.pate_chinois, R.drawable.ploye_acadienne_crepes_sirop_erable,
            R.drawable.poisson_cru, R.drawable.poisson_fruits_de_mer_bouillabaisse_des_iles_de_la_madeleine, R.drawable.poutine_frites,
            R.drawable.queues_de_castor, R.drawable.sirop_erable_canada, R.drawable.soupe_aux_pois, R.drawable.tarte_aux_baies_saskatoon, R.drawable.tartelettes_au_beurre_sirop_erable,
            R.drawable.tourtiere_tarte_viande, R.drawable.viande_ragout_pattes_de_cochon};

    public Resources res;

    private Button boutonSuivant;
    ListView listViewRepas;

    public String repasSelectedDataBase = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1);

         Intent i = getIntent();
         identifiant = i.getIntExtra(ID,0);

         maBase = new DataBase(this);


        res = getResources();
        repas = res.getStringArray(R.array.liste_repas);

        repasSelected = new boolean[repas.length];
        repasSelectedDataBase = maBase.getQuestion1(identifiant);
        repasSelected = convertRepasSelectedDataBase();

        textView2=findViewById(R.id.idQuestion1);
        textView3=findViewById(R.id.idBoutonSuivant);
        taille = i.getIntExtra(TAillE,0);
        //setTextSize(taille);


        //index = new int[repas.length];

        listViewRepas = (ListView) findViewById(R.id.idListViewPage1);
        final Adaptateur adaptateur = new Adaptateur(this,images,repas);
        listViewRepas.setAdapter(adaptateur);

        adaptateur.updateRecords(repasSelected);

        //Toast.makeText(ActivityPage1.this, "Reponse = " + repasSelectedDataBase, Toast.LENGTH_LONG).show();

        listViewRepas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                boolean etatRepas = repasSelected[position];
                if(etatRepas)
                    repasSelected[position] = false;
                else
                    repasSelected[position] = true;
                adaptateur.updateRecords(repasSelected);
                //repasSelectedDataBase = maBase.getQuestion1(identifiant);

                //Toast.makeText(ActivityPage1.this, "Reponse = " + repasSelectedDataBase, Toast.LENGTH_LONG).show();
                //getCheckboxCheckedListner(position);
                //Toast.makeText(ActivityPage1.this, "Repas = " + position, Toast.LENGTH_LONG).show();
                //select = findViewById(R.id.idSelect);
                //select.setText("OK");
            }
        });


        boutonSuivant = findViewById(R.id.idBoutonSuivant);
        boutonSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chaineRepas = convertRepasSelected();
                 boolean b = maBase.updateQuestion1(identifiant, chaineRepas);
                 boolean b2 = maBase.updatePageCourante(identifiant, 2);
                Intent intent = new Intent(ActivityPage1.this, ActivityPage2.class);
               // intent.putExtra(ActivityPage2.TAillE, taille);
                intent.putExtra(ActivityPage2.ID, identifiant);
                startActivity(intent);
            }
        });
    }
    public String convertRepasSelected(){
        String chaine = "";
        for(int i=0; i<repasSelected.length; i++){
            if(repasSelected[i] == true){
                chaine += (i+" ");
            }
        }
        return chaine;
    }

   public boolean[] convertRepasSelectedDataBase(){
        boolean [] tab = new boolean[repasSelected.length];

        if(repasSelectedDataBase.length() > 0){
            String [] tab2 =repasSelectedDataBase.split(" ");
            if(tab2.length > 0){
                int[] tab3 = new int[tab2.length];
                for(int i=0; i<tab3.length; i++){
                    tab3[i] = Integer.parseInt(tab2[i]);
                    // tab3[i] = 1;
                }

                for(int i=0; i<tab3.length; i++){
                    tab[tab3[i]] = true;
                }
            }
        }

        return tab;
    }

    public void prochainpage() {
        Intent intent = new Intent(this, ActivityPage2.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            // finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;

        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }

    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTextSize(int textSize) {
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }

}