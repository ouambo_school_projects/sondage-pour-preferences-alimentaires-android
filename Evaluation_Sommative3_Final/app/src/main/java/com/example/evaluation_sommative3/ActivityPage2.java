package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityPage2 extends AppCompatActivity {

    public static final String ID="";
    public int identifiant;
    public static String TAillE ="";
    private int taille = 26;
    TextView textView;
    TextView textView2;
    TextView textView3;

    DataBase maBase;

    private String[] province;

    private int[] images = {R.drawable.alberta, R.drawable.colombie, R.drawable.ile_du_prince_edward,
            R.drawable.manitoba,R.drawable.nouveau_bruswick, R.drawable.nouvelle_ecosse,
            R.drawable.nunavut, R.drawable.ontario, R.drawable.quebec, R.drawable.sasketchewan, };

    public Resources res;

    private  AdaptateurPage2 adaptateur;

    private Button boutonPrecedent, boutonSuivant;

    private boolean[] provinceSelected;

    private int provinceSelectedDataBase;

    GridView gridViewProvince;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);

        Intent i = getIntent();
        identifiant = i.getIntExtra(ID,0);

        maBase = new DataBase(this);

        res = getResources();
        province = res.getStringArray(R.array.province);

        provinceSelected = new boolean[province.length];
        provinceSelectedDataBase = maBase.getQuestion2(identifiant);
        provinceSelected = getProvinceSelected();

        gridViewProvince = findViewById(R.id.idGridViewPage2);

        adaptateur = new AdaptateurPage2(this, images, province);
        gridViewProvince.setAdapter(adaptateur);

        adaptateur.updateRecords(provinceSelected);

        textView=findViewById(R.id.idQuestion2);
        textView2=findViewById(R.id.idBoutonPrecedentPage2);
        textView3=findViewById(R.id.idBoutonSuivantPage2);
        taille = i.getIntExtra(TAillE,0);
        //setTextSize(taille);


        //Toast.makeText(ActivityPage2.this, "province = " + provinceSelectedDataBase, Toast.LENGTH_LONG).show();

        boutonPrecedent = findViewById(R.id.idBoutonPrecedentPage2);
        boutonSuivant = findViewById(R.id.idBoutonSuivantPage2);


        gridViewProvince.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean etatProvence = provinceSelected[position];

                if(!etatProvence){
                    provinceSelected[position] = true;
                    for(int k=0; k<provinceSelected.length; k++){
                        if(k != position)
                            provinceSelected[k] = false;
                    }
                    adaptateur.updateRecords(provinceSelected);
                }
            }
        });


        boutonPrecedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndicereponse();
                boolean b = maBase.updateQuestion2(identifiant, indiceReponse);
                boolean b2 = maBase.updatePageCourante(identifiant, 3);
                Intent intent = new Intent(ActivityPage2.this, ActivityPage1.class);
                //intent.putExtra(ActivityPage1.TAillE, taille);
                intent.putExtra(ActivityPage1.ID, identifiant);
                startActivity(intent);
            }
        });

        boutonSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indiceReponse = getIndicereponse();
                boolean b = maBase.updateQuestion2(identifiant, indiceReponse);
                Intent intent = new Intent(ActivityPage2.this, ActivityPage3.class);
                //intent.putExtra(ActivityPage3.TAillE, taille);
                intent.putExtra(ActivityPage3.ID, identifiant);
                startActivity(intent);
            }
        });
    }

    public int getIndicereponse(){
        int ind = -1;
       for(int j = 0; j < provinceSelected.length; j++){
           if(provinceSelected[j] == true){
               ind = j;
           }
        }
        return ind;
    }

    public boolean[] getProvinceSelected(){
        boolean[] tab = new boolean[provinceSelected.length];
        tab = provinceSelected;
        for(int k = 0; k< tab.length; k++){
            if(k == provinceSelectedDataBase)
                tab[k] = true;
        }
        return tab;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            // finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;

        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }

    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTextSize(int textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }
}
