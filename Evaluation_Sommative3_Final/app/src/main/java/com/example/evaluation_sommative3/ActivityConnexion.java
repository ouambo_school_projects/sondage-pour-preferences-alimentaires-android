package com.example.evaluation_sommative3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ActivityConnexion extends AppCompatActivity {

    public static String EMAIL ="";
    public static String TAillE ="";
    public EditText email, password;

    DataBase maBase;

    private int taille = 26;
    TextView textView2;
    TextView textView3;


    Button exitbutton;
    TextView  textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

         Intent i = getIntent();

        email = findViewById(R.id.textInputEditText);
        email.setText(i.getStringExtra(EMAIL));


        maBase = new DataBase(this);

        textView = findViewById(R.id.textView3);

        textView2=findViewById(R.id.textView7);

        textView3=findViewById(R.id.textView9);


        taille = i.getIntExtra(TAillE,0);
        setTextSize(taille);


    }

    public void CreerCompte(View view) {
        Intent intent = new Intent(ActivityConnexion.this, ActivityCreerCompte.class);
        intent.putExtra(ActivityConnexion.TAillE, taille);
        startActivity(intent);
    }

    public void Connecter(View view) {
        email = findViewById(R.id.textInputEditText);
        password = findViewById(R.id.textInputEditText2);

       if(!maBase.verifierIdentite(email.getText().toString().trim(),
                password.getText().toString().trim()))
            Toast.makeText(this,"Email ou mot de passe incorrect",
                    Toast.LENGTH_SHORT).show();
        else{
                int id = maBase.getIdVisiteur(email.getText().toString().trim(), password.getText().toString().trim());

                int page = maBase.getPageCourante(id);

                switch (page){
                    case 1:
                        Intent  i = new Intent(this, ActivityPage1.class);
                        //i.putExtra(ActivityPage1.TAillE, taille);
                        i.putExtra(ActivityPage1.ID, id);
                        startActivity(i);
                        break;
                    case 2:
                        Intent  i2 = new Intent(this, ActivityPage2.class);
                        i2.putExtra(ActivityPage2.ID, id);
                        //i2.putExtra(ActivityPage1.TAillE, taille);
                        startActivity(i2);
                        break;
                    case 3:
                        Intent  i3 = new Intent(this, ActivityPage3.class);
                        i3.putExtra(ActivityPage3.ID, id);
                        //i3.putExtra(ActivityPage1.TAillE, taille);
                        startActivity(i3);
                        break;
                    case 4:
                        Intent  i4 = new Intent(this, ActivityPage4.class);
                        i4.putExtra(ActivityPage4.ID, id);
                        //i4.putExtra(ActivityPage1.TAillE, taille);
                        startActivity(i4);
                        break;
                    case 5:
                        Intent  i5 = new Intent(this, ActivityPage5.class);
                        i5.putExtra(ActivityPage5.ID, id);
                        //i5.putExtra(ActivityPage1.TAillE, taille);
                        startActivity(i5);
                        break;
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sondage, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.exitbutton) {
            // Perform action to close the application
            // finish(); // Call finish() to close the current activity
            System.exit(0); // Call System.exit(0) to terminate the application process
            return true;
        } else if (id == R.id.buttonPetit) {
            // Change text size to small
            taille = 14;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonMoyen) {
            // Change text size to medium
            taille = 18;
            setTextSize(taille);
            return true;
        } else if (id == R.id.buttonGrand) {
            // Change text size to large
            taille = 22;
            setTextSize(taille);
            return true;

        } else if (id== R.id.description) {

            showDescriptionDialog();

        } else if (id== R.id.membres) {

            showDescriptionDialog1();
        }


        return super.onOptionsItemSelected(item);
    }

    private void showDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Description")
                .setMessage(R.string.Descritpion) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDescriptionDialog1() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Membres")
                .setMessage(R.string.Membres) // Set your dialog message here
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Perform any action on OK button click
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTextSize(int textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textView3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }
}