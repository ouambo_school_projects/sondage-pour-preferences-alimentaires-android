package com.example.evaluation_sommative3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DataBaseSondage.db";
    public static final String TABLE_NAME = "Visiteur";
    public static final String COL_1 = "ID_VISITEUR";
    public static final String COL_2 = "NOM_PRENOM";
    public static final String COL_3 = "EMAIL";
    public static final String COL_4 = "PHONE";
    public static final String COL_5 = "PASSWORD";
    public static final String COL_6 = "QUESTION1";
    public static final String COL_7 = "QUESTION2";
    public static final String COL_8 = "QUESTION3";
    public static final String COL_9 = "QUESTION4";
    public static final String COL_10 = "QUESTION5";
    public static final String COL_11 = "PAGE_COURANTE";

    public DataBase(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
     db.execSQL("create table " + TABLE_NAME + "(" + COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                                 + COL_2 + " TEXT, "
                                                 + COL_3 + " TEXT, "
                                                 + COL_4 + " INTEGER, "
                                                 + COL_5 + " TEXT ,"
                                                 + COL_6 + " TEXT ,"
                                                 + COL_7 + " INTEGER ,"
                                                 + COL_8 + " INTEGER ,"
                                                 + COL_9 + " INTEGER ,"
                                                 + COL_10 + " TEXT ,"
                                                 + COL_11 + " INTEGER)"
     );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Visiteur");
    }


    public boolean insererVisiteur(String nom, String email, String phone, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, nom);
        contentValues.put(COL_3, email);
        contentValues.put(COL_4, phone);
        contentValues.put(COL_5, password);
        contentValues.put(COL_5, password);
        contentValues.put(COL_6, "");
        contentValues.put(COL_7, -1);
        contentValues.put(COL_8, -1);
        contentValues.put(COL_9, "");
        contentValues.put(COL_10, "");
        contentValues.put(COL_11, 1);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
            return false;
        return true;
    }

    public boolean verifierEmail(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_3 + " = ? ",
                                       new String[]{email});
        return cursor.getCount() > 0 ? true : false;
    }

    public boolean verifierIdentite(String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_3 + " = ? "+
                " and " + COL_5 + " = ? ", new String[] {email, password});
        return cursor.getCount() > 0 ? true : false;
    }

    public int getIdVisiteur(String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_3 + " = ? "+
                " and " + COL_5 + " = ? ", new String[] {email, password});
        int id = -1;
        if(cursor.moveToFirst()){
           id = cursor.getInt(0);
       }
        cursor.close();
        db.close();
        return id;
    }


    public boolean updateQuestion1(int id, String reponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_6, reponse);
        db.update(TABLE_NAME, values, COL_1 + " = " + id, null);
        db.close();
        return true;
    }

    public boolean updateQuestion2(int id, int reponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_7, reponse);
        db.update(TABLE_NAME, values, COL_1 + " = " + id, null);
        db.close();
        return true;
    }

    public boolean updateQuestion3(int id, int reponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_8, reponse);
        db.update(TABLE_NAME, values, COL_1 + " = " + id, null);
        db.close();
        return true;
    }

    public boolean updateQuestion4(int id, int reponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_9, reponse);
        db.update(TABLE_NAME, values, COL_1 + " = " + id, null);
        db.close();
        return true;
    }

    public boolean updateQuestion5(String email, String password, String reponse){
        return true;
    }

    public boolean updatePageCourante(int id, int page){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COL_11, page);
        db.update(TABLE_NAME, values, COL_1 + " = " + id, null);
        db.close();
        return true;
    }


    public String getQuestion1(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_1 + " = " + id, null);
        String question = "";
        if(cursor.moveToFirst()){
            question = cursor.getString(5);
        }
        return question;
    }

    public int getQuestion2(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_1 + " = " + id, null);
        int question = -1;
        if(cursor.moveToFirst()){
            question = cursor.getInt(6);
        }
        return question;
    }

    public int getQuestion3(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_1 + " = " + id, null);
        int question = -1;
        if(cursor.moveToFirst()){
            question = cursor.getInt(7);
        }
        return question;
    }

    public int getQuestion4(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_1 + " = " + id, null);
        int question = -1;
        if(cursor.moveToFirst()){
            question = cursor.getInt(8);
        }
        return question;
    }

    public String getQuestion5(String email, String password){
        return "";
    }

    public int getPageCourante(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_1 + " = " + id, null);
        int page = -1;
        if(cursor.moveToFirst()){
            page = cursor.getInt(10);
        }
        return page;
    }
}
